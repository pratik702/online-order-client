import { Environment } from '../app/common/models/environment.model'

export const environment : Environment= {
  environmentName: 'production',
  production: true,
  // api: 'http://localhost:8080'
  api: 'https://fast-island-38275.herokuapp.com'
};