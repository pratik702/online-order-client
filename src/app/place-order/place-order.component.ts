import { Component, OnInit, EventEmitter, ViewChildren, QueryList, ElementRef } from '@angular/core';
import { ApiService } from '../common/service/api.service.';
import { NotificationService } from '../common/service/notification.service';
import { Menu } from '../common/models/menu.model';
import { OrderItem } from '../common/models/order-item.model';
import { TenderDetail } from '../common/models/tender.model';
import { OrderBook } from '../common/models/order-book.model';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-place-order',
  templateUrl: './place-order.component.html',
  styleUrls: ['./place-order.component.scss']
})
export class PlaceOrderComponent implements OnInit {

  menus: Array<Menu> = [];
  loadedOrderItems: Array<OrderItem> = [];
  paymentForm: FormGroup;
  paymentOptions = ['Cash', 'Credit Card', 'Gift Card', 'Travel Cheque', 'Coupon']
  @ViewChildren("checkboxes") checkboxes: QueryList<ElementRef>;

  value;

  constructor(
    private apiService: ApiService,
    private notificationService: NotificationService
  ) { }

  ngOnInit(): void {
    this.paymentForm = new FormGroup({
      customerName: new FormControl('', [
      ]),
      customerPhone: new FormControl('', [
      ]),
      paymentMedium: new FormControl('', [
        Validators.required
      ]),
      paymentAmount: new FormControl('', [
        Validators.required
      ]),
    });
    this.apiService.getAllMenus().then(response =>{
      this.menus = response
      this.menus.forEach(menu => {
        this.loadedOrderItems.push(new OrderItem(menu,0))
      })
    }).catch(err => {
      console.log("Unable to load menu items")
    })
  }

  // checkBoxClick(orderItem: OrderItem, isChecked: boolean){
  //   if (isChecked){
  //     this.orderItems.push(orderItem)
  //   }
  //   else{
  //     this.orderItems = this.orderItems.filter(item => item.menu.name != orderItem.menu.name)
  //   }
  // }

  getTotalBill(){
    let total: number = 0;
    for (let item of this.getOrderItems()){
      total+= item.menu.price * item.quantity
    }
    return total
  }

  getNumberOfOrderItems(){
    return this.loadedOrderItems.reduce(function(previousValue, currentObject){
      return previousValue + (currentObject.quantity > 0 ? 1: 0); 
  }, 0);
  }

  isOrderValid(): boolean{
    for (let item of this.loadedOrderItems){
      if (item.quantity > 0){
        return true;
      }
    }
    return false;
  }



  confirmPayment(){
    let paymentMedium = this.paymentForm.controls.paymentMedium.value;
    let paymentAmount = this.paymentForm.controls.paymentAmount.value;
    let customerName = this.paymentForm.controls.customerName.value;
    let customerPhone = this.paymentForm.controls.customerPhone.value;

    let tenderDetail = new TenderDetail(paymentAmount, paymentMedium);
    let orderBook = new OrderBook();
    orderBook.customerName = customerName;
    orderBook.customerPhone = customerPhone;
    orderBook.orderItems = this.getOrderItems();
    orderBook.tenderDetails = tenderDetail;

    this.apiService.placeOrder(orderBook).then(response => {
      this.clearValues();
      this.notificationService.success("Success", "Order placed");
    }).catch(err => {
      this.notificationService.error("Failed", "Unable to place order.")
    })
  }

isPaidAmountValid(): boolean{
  if (this.paymentForm.controls.paymentAmount.value < this.getTotalBill()){
    return false
  }
  return true
}

handleMinus(index) {
  this.loadedOrderItems[index].quantity = this.loadedOrderItems[index].quantity - 1 >= 0 ? this.loadedOrderItems[index].quantity - 1 : 0
}

handlePlus(index) {
  this.loadedOrderItems[index].quantity = this.loadedOrderItems[index].quantity + 1
}

getOrderItems(){
  return this.loadedOrderItems.filter(item => item.quantity > 0)
}

clearValues() {
  this.paymentForm.controls.paymentMedium.setValue('');
  this.paymentForm.controls.paymentAmount.setValue('');
  this.paymentForm.controls.customerName.setValue('');
  this.paymentForm.controls.customerPhone.setValue('');
  this.checkboxes.forEach((element) => {
    element.nativeElement.checked = false;
  });
  this.loadedOrderItems.forEach(element =>{
    element.quantity = 0;
  })
}

}
