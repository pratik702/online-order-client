import { Component, OnInit } from '@angular/core';
import { ApiService } from '../common/service/api.service.';
import { OrderBook } from '../common/models/order-book.model';
import { ElementSchemaRegistry } from '@angular/compiler';
import { OrderItem } from '../common/models/order-item.model';

@Component({
  selector: 'app-view-orders',
  templateUrl: './view-orders.component.html',
  styleUrls: ['./view-orders.component.scss']
})
export class ViewOrdersComponent implements OnInit {

  orderBooks: Array<OrderBook> = [];
  loading: boolean = true;

  constructor(
    private apiService: ApiService
  ) { }

  ngOnInit(): void {
    this.apiService.getAllOrderBooks().then(res => {
      this.orderBooks = res;
      this.orderBooks.forEach(item =>
        item.visible = false)
      this.loading = false;
    }).catch(err => {
      console.log(err)
      this.loading = false;
    })
  }

  showChild(index){
    this.orderBooks[index].visible = !this.orderBooks[index].visible
  }

  getTotalBill(orderItems: Array<OrderItem>): number{
    let totalAmount = 0;
    for (let orderItem of orderItems){
        totalAmount = totalAmount + (orderItem.quantity* orderItem.menu.price)
    }
    return totalAmount;
  }

}
