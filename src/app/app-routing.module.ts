import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { PlaceOrderComponent } from './place-order/place-order.component';
import { ViewOrdersComponent } from './view-orders/view-orders.component';
import { AuthGuard } from './common/service/auth-guard.service';

const routes: Routes = [
  {path:  "", pathMatch:  "full",redirectTo:  "/dashboard", canActivate: [AuthGuard]},
  {path: "login", component: LoginComponent},
  {path: "dashboard", component: DashboardComponent, canActivate: [AuthGuard] },
  {path: "place-order", component: PlaceOrderComponent, canActivate: [AuthGuard] },
  {path: "view-orders", component: ViewOrdersComponent, canActivate: [AuthGuard] }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
