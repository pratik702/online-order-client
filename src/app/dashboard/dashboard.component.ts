import { Component, OnInit } from '@angular/core';
import { ApiService } from '../common/service/api.service.';
import { Router } from '@angular/router';
import { DashboardDTO } from '../common/models/dashboardDTO.model';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  dashboardData: DashboardDTO;

  constructor(
    private router: Router,
    private apiService: ApiService
    ) { }

  ngOnInit(): void {
    this.apiService.getDashboardData().then(res => {
      this.dashboardData = res;
      console.log(this.dashboardData)
    })
  }

  navigateToPlaceOrder(){
    this.router.navigate(['/place-order'])
  }

}
