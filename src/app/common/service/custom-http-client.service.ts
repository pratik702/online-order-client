import { Observable } from 'rxjs';
import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpParams, HttpRequest, HttpEventType, HttpHeaders, HttpResponse } from '@angular/common/http';

import { Environment } from 'src/app/common/models/environment.model';
import { Option } from  'src/app/common/models/option.model';

@Injectable()
export class CustomHttpClient {

    constructor(
        private http: HttpClient,
        @Inject('@@environment') private environment: Environment) {
    }

    get<T>(url: string, options?: Option): Promise<any> {
        options = this.setAuthorizationHeader(options)
        return this.http.get<T>(this.environment.api + url, options).toPromise();
    }

    post(url: string, body: any | null, options?: Option): Promise<any> {
        options = this.setAuthorizationHeader(options)
        return this.http.post(this.environment.api + url, body, options).toPromise();
    }

    setAuthorizationHeader(options): Option{
        let auth = localStorage.getItem('Authorization');
        if (auth){
            if (options){
                if (options.headers){
                    options.headers.set('Authorization', auth)
                }
                else{
                    options.headers = new HttpHeaders().set('Authorization', auth)
                }
            }
            else{
                let auth = localStorage.getItem('Authorization');
                if (auth){
                    options = {headers: new HttpHeaders().set('Authorization', auth)}
                }
            }
        }
        return options
    }

}
