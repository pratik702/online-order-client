import { Injectable, Inject } from "@angular/core";
import {
    HttpClient,
    HttpParams,
    HttpRequest,
    HttpEventType,
    HttpHeaders,
    HttpResponse,
  } from "@angular/common/http";
import { Environment } from 'src/app/common/models/environment.model';
import { User } from 'src/app/common/models/user.model';
import { CustomHttpClient } from './custom-http-client.service';
import { OrderBook } from '../models/order-book.model';
import { DashboardDTO } from '../models/dashboardDTO.model';

@Injectable()
export class ApiService{

    constructor(private http: CustomHttpClient) { }

    userLogin(user: User): Promise<any>{
        return this.http.post('/login', user, {observe: "response" as 'body'});
    }

    getAllMenus(): Promise<Array<any>>{
        return this.http.get('/order/menus');
    }

    placeOrder(orderBook: OrderBook): Promise<any>{
        return this.http.post('/order/place-order', orderBook);
    }

    getDashboardData(): Promise<DashboardDTO>{
        return this.http.get('/order/dashboard');
    }

    getAllOrderBooks(): Promise<Array<OrderBook>>{
        return this.http.get('/order/orderbooks');
    }

}