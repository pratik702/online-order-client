export class DashboardDTO{
    numberOfOrders: number;
    mostOrderedItem: String;
    mostOrderedItemCount: number;
    totalAmountCollected: number;
}