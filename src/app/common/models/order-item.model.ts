import { Menu } from '../models/menu.model';
export class OrderItem{
    menu: Menu;
    quantity: number;
    constructor(menu: Menu, quantity: number){
        this.menu = menu;
        this.quantity = quantity;
    }
}