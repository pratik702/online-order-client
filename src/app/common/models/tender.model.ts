export class TenderDetail{
    amount: number;
    paymentMedium: String;

    constructor(amount: number, paymentMedium: String){
        this.amount = amount;
        this.paymentMedium = paymentMedium;
    }
}
