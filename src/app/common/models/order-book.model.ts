import { OrderItem } from '../../common/models/order-item.model';
import { TenderDetail } from '../../common/models/tender.model';

export class OrderBook{
    customerName: String;
    customerPhone: String;
    orderItems: Array<OrderItem>;
    tenderDetails: TenderDetail;
    visible: boolean;
}