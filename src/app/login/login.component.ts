import { Component, OnInit } from '@angular/core';
import { Router, PreloadingStrategy } from '@angular/router';
import {ApiService} from 'src/app/common/service/api.service.'
import { User } from 'src/app/common/models/user.model';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { NotificationService } from '../common/service/notification.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;

  constructor(private router: Router,
    private apiService: ApiService,
    private notificationService: NotificationService) { }

  ngOnInit(): void {
    this.loginForm = new FormGroup({
      userName: new FormControl('', [
        Validators.required
      ]),
      password: new FormControl('', [
        Validators.required
      ]),
    });
  }

  login(){
    let userName = this.loginForm.controls.userName.value;
    let password = this.loginForm.controls.password.value;
    if (userName && userName != '' && password && password !=''){
      let  user: User = { userName, password};
      this.apiService.userLogin(user).then(res => {
      localStorage.setItem('Authorization', res.headers.get('Authorization'))
      this.router.navigate(['/dashboard']);
    }).catch(err => {
      this.notificationService.error('Failure', 'Unable to login')
    })
    }
    else{
      this.notificationService.warn('Invalid','User name or password not valid');
    }
  }

}
