import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { HttpClientModule } from '@angular/common/http';
import { ApiService } from 'src/app/common/service/api.service.';
import { CustomHttpClient } from 'src/app/common/service/custom-http-client.service';
import { environment } from 'src/environments/environment';
import { PlaceOrderComponent } from './place-order/place-order.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { ToastrModule } from 'ngx-toastr';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NotificationService } from './common/service/notification.service';
import { AuthGuard } from './common/service/auth-guard.service';
import { ViewOrdersComponent } from './view-orders/view-orders.component';


const PROVIDE_ENVIRONMENT = '@@environment';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    DashboardComponent,
    PlaceOrderComponent,
    ViewOrdersComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    FormsModule,
    ToastrModule.forRoot()
  ],
  providers: [
    ApiService,
    CustomHttpClient,
    NotificationService,
    AuthGuard,
    { provide: PROVIDE_ENVIRONMENT, useValue: environment }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
