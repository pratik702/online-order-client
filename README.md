# Online Order Client

**Notes:**
1. online-order-client is also available running at http://shrouded-retreat-40978.herokuapp.com
2. online-order-service is also available running at https://fast-island-38275.herokuapp.com

**Credentials:**
1. Username: pratik702
2. Password: josh

**Prerequisites:**
1. Node Package manager should be installed. 
2. Latest Angular CLI should be installed
2. online-order-service should be running. (https://gitlab.com/pratik702/online-order)

**Steps to setup:**

1. Clone this project.
2. Open command prompt and traverse to the project directory.
3. Execute this command to download project dependencies: npm install
4. Execute this command to run the project: ng serve



